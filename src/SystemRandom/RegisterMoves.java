/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SystemRandom;

import java.io.IOException;
import java.io.RandomAccessFile;


/**
 * Este clase es la encargada de registrar lo movimientos del sistema en el archivo aleatorio
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class RegisterMoves extends SystemMove {


    public final static int TAM = 30;//tamaño del archivo


    public RegisterMoves() {
    }


    public RegisterMoves(int row, int column, String time, char caseContent) {
        super(row, column, time, caseContent);
    }

       
    /**
     * este metodo se encarga de leer el registro y guardarlo en SystemMove
     * @param file. recibe el nombre del archivo
     */
    public void readRegister(RandomAccessFile file) {
        try {

            setRow(file.readInt()); //4
            setColumn(file.readInt()); //4
            setTime(readString(file, 10)); //20
            setBoxContent(file.readChar()); //2

        } catch (IOException e) {
            System.err.println("Error en leerRegistro\n" + e);
        }
    }

    /**
     * escribe el registro de archivo aleatorio
     * @param file recibe el nombre del archivo 
     */
    public void writeRegister(RandomAccessFile file) {
        try {
            file.writeInt(getRow()); //4
            file.writeInt(getColumn()); //4
            writeString(file, getTime(), 10); //20
            file.writeChar(getBoxContent()); //2

        } catch (IOException e) {
            System.err.println("Error en escribirRegistro\n" + e);
        }
    }
    

    /**
     * Este es el metodo implementado en el readRegister para leer los String de este
     * @param file. nombre del archivo aleatorio
     * @param tam. tamaño del String que esta dentrto del archivo
     * @return retorna una cadena del String
     */
    private String readString(RandomAccessFile file, int tam) {
        char campo[] = new char[tam];
        
        try {
            for(int i = 0; i < tam; i++) {
                campo[i] = file.readChar();
            }
          } catch (IOException e) {
            System.err.println("Error en leerString\n" + e);
        }
        String hilera = new String(campo);
        hilera = hilera.trim();
        return hilera;
    }
    
    /**
     * Este es el metodo implementado en el writeRegister para escribir un String dentro de un registro
     * @param file nombre del archivo aleatorio
     * @param str Recibe el String a escribir
     * @param tam tamaño del String que esta dentrto del archivo
     */
    private void writeString(RandomAccessFile file, String str, int tam) {
        StringBuilder buffer = new StringBuilder();
        if (str != null) {
            buffer.append(str);
        }
        buffer.setLength(tam);
        try {
            file.writeChars(buffer.toString()); //Escribe al archivo
        } catch (IOException e) {
            System.err.println("Error en escribirString\n" + e);
        }
    }

}
