/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SystemRandom;




import ObjectCreator.ObjectCreator;
import SystemRandom.SystemMoveCreator;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Esta clase es el manejador del Registro la cual se encarga de escribir leer abrir y cerrar el fichero
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class RegisterMovesHandler {


    // Fichero de acceso aleatorio
    private RandomAccessFile file;

    /**
     * Metodo encargado de abrir el arhivo aleatorio
     * @param name Nombre del fichero
     * @return retorna una validacion que indica si abrio o no el fichero aleatorio
     */
    public boolean open(String name) {
        try {
            file = new RandomAccessFile(name, "rw");//crea el fichero y el rw dice que es de escritura y lectura
            return true;
        } catch (IOException e) {
            System.err.println("Error en abrir\n" + e);
            return false;
        }
    }

    /**
     * Metodo que cierra el fichero 
     */
    public void close() {
        try {
            if (file != null) {
                file.close();
            }
        } catch (IOException e) {
            System.err.println("Error en cerrar\n" + e);
        }
    }

    /**
     * metodo que escribe un registro en la posicion actual del cursor
     * @param register recibe un registro por parametros para llamar el metodo de escribir el registro
     */
    public void write(RegisterMoves register) {
        if (file != null) {
            register.writeRegister(file);
        }
    }

    // Escribir un registro en una posición cualquiera

    /**
     * metodo que escribe un registro en la posicion elegida por el usuario
     * @param register recibe un registro por parametros para llamar el metodo de escribir el registro
     * @param pos recibe la posicion en la cual escribir
     */
    public void write(RegisterMoves register, int pos) {
        try {
            if (file != null) {

                file.seek(pos * RegisterMoves.TAM);
                write(register);
            }
        } catch (IOException ex) {
            Logger.getLogger(RegisterMovesHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        write(register);
    }
        
    /**
     * metodo que lee el fichero el registro que se encuentra en la posicion actual del cursor
     * @return register retorna el registro 
     */
    public RegisterMoves read() {
        RegisterMoves register = null;
        if (file != null) {
            register = new RegisterMoves();
            register.readRegister(file);
        }
        return register;
    }

    // Leer del fichero un registro cualquiera
    // (el parametro indica la posicion del registro)
    /**
     * metodo que Lee el fichero un registro cualquiera
     * @param pos indica la posicion del registro
     * @return nuevo metodo para leer 
     */
    public RegisterMoves read(int pos) {
        try {
            if (file != null) {
                file.seek(pos * RegisterMoves.TAM);
            }
        } catch (Exception e) {
            System.out.println("error" + e);
        }
        return read();
    }

    /**
     * Metodo que calcula la cantidad de registros
     * @return cantidad de registros
     */
    public int quantityRegister() {
        int numRegister = 0;
        try {
            if (file != null) {
                numRegister = (int) file.length() / RegisterMoves.TAM;
            }
        } catch (IOException e) {
            System.err.println("Error en cantidadRegistros\n" + e);
        }
        return numRegister;
    }
    
    /**
     * Metodo para modificar el tiempo en los movimientos del usuario
     * en cada registro
     * @param newtime recibe el tiempo en que se efectua el movimiento
     */

    public void resetTime(String newtime, int row, int column, char newChar){
        try {
            for (int i = 0; i < quantityRegister(); i++) {
                RegisterMoves temp = read(i);
                if ((temp.getRow() == row) && (temp.getColumn() == column)) {

                    temp.setTime(newtime);
                    temp.setBoxContent(newChar);
                    write(temp);
                    
                    break;
                }

            }

        } catch (Exception e) {
        }

    }
    
    /**
     * Metodo que modifica el contenido de ls casilla segun lo encontrado al 
     * hacer el movimiento
     * @param newChar indica lo que contiene la matriz en el la posicion enviada
     * @param row fila de la matriz
     * @param column columna de la matriz
     */
    public void resetChar(int row, int column, char newChar){
        try {
            for (int i = 0; i < quantityRegister(); i++) {
                RegisterMoves temp = read(i);
                if ((temp.getRow() == row) && (temp.getColumn() == column)) {
                    temp.setBoxContent(newChar);
                    write(temp);
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("error" + e);
        }
    }

    
    /**
     * Inprime todos los registros y su contenido
     * @return una lista con la informacion de todos los registros
     */

    public String deployAllRegisters() {

        String str = "";
        int num = 1;
        for (int i = 0; i < quantityRegister(); i++) {
            str += num + ")  " + read(i).toString() + "\n";
            num++;
        }
        return str;
    }


    /**
     * Metodo que llena 225 registros aleatorios vacios vacios
     * para los ataques del sistema
     */
    public void CreateEmptyRegisters() {
        ObjectCreator create = new ObjectCreator();
        RegisterMoves register;
                
            for (int i = 0; i < 15; i++) {
                for (int j = 0; j < 15; j++) {
                    write(create.createMoves(i, j));
                }
            }
    }
}
