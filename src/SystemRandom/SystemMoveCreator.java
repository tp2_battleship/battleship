/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemRandom;

/**
 * Esta clase crea un nuevo registro con valores por defecto
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class SystemMoveCreator {
    
    
    
    
    public SystemMoveCreator() {
    }

    /**
     * metodo que crea el registro inicial
     */
    public void createDefaultRegisty() {
        int row = 0;
        int column = 0;
        String time = "0";
        char boxContent = ' ';
            
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                
                row = i;
                column = j;
                
                RegisterMoves register = new RegisterMoves(row, column, time, boxContent);
            }
        }

    }

}
