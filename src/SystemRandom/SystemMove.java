/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemRandom;
/**
 * Esta clase contiene los datos necesarios para el manejo del movimiento del sistema
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class SystemMove {
  private int row;
  private int column;

  private String time;

  private char boxContent;

    public SystemMove() {
    }


    public SystemMove(int row, int column, String time, char caseContent) {

        this.row = row;
        this.column = column;
        this.time = time;
        this.boxContent = caseContent;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public char getBoxContent() {
        return boxContent;
    }

    public void setBoxContent(char caseContent) {
        this.boxContent = caseContent;
    }
       
    
    @Override
    public String toString() {
        return "SystemMove{" + "row=" + row + ", column=" + column + ", time=" + time + ", caseContent=" + boxContent + '}';
    }

    
  
     
}
