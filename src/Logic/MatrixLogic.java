package Logic;

import Exceptions.MyException;
import java.util.Random;
/**
 * Esta clase es la encargada de manejar las 2 matrices tanto la del sistema como la
 * del usuario y todos los metodos necesarios para cargar y jugar battleship
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class MatrixLogic {

    private static MatrixLogic matrixLogic = new MatrixLogic();//singleton
    private char[][] boardMatrix = new char[15][15];//matriz del usuario
    private char[][] boardMatrixSystem = new char[15][15];//matriz del sistema
    public static int countHits = 14; //contador de golpes
    private int rowSys; //fila del sistema
    private int columnSys;//columna del sistema

    private MatrixLogic() {
    }

    public MatrixLogic(char[][] matrix) {
        this.boardMatrixSystem = matrix;
    }

    /**
     * Este metodo es para realizar la unica instancia de esta clase
     *
     * @return matrixLogic. instance de la clase
     */
    public static MatrixLogic getInstance() {
        if (matrixLogic == null) {
            MatrixLogic matrixLogic = new MatrixLogic();

        }
        return matrixLogic;
    }

    public int getRowSys() {
        return rowSys;
    }

    public void setRowSys(int rowSys) {
        this.rowSys = rowSys;
    }

    public int getColumnSys() {
        return columnSys;
    }

    public void setColumnSys(int columnSys) {
        this.columnSys = columnSys;
    }


    public char[][] getBoardMatrix() {
        return boardMatrix;
    }

    public void setBoardMatrix(char[][] boardMatrix) {
        this.boardMatrix = boardMatrix;
    }

    public char[][] getBoardMatrixSystem() {
        return boardMatrixSystem;
    }

    public void setBoardMatrixSystem(char[][] boardMatrixSystem) {
        this.boardMatrixSystem = boardMatrixSystem;
    }

    public int getCountHits() {
        return countHits;
    }

   /**
    * Este metodo el que retorna la matriz del usuario
    * @return string. retorna un string con el usuario
    */
    public String getStringMatrix() {
        String string = "";
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                string += boardMatrix[row][column] + " ";
            }
            string += "\n";
        }
        return string;
    }
    /**
     * Este metodo retorna la matriz del usuario
     * @return int retorna la matriz
     */
    public int getRowSize() {
        return boardMatrix.length;
    }
    /**
     * Este metodo retorna el tamaño de las columnas
     * @return int to board matrix
     */
    public int getColumnSize(int row) {
        return boardMatrix[row].length;
    }

    /**
     * Este es el metodo encargado de añadir un elemento a la matriz del usuario
     * @param row
     * @param column
     * @param element 
     */
    public void addElement(int row, int column, char element) {
        if ((row >= 0) && (column >= 0) && (row < boardMatrix.length) && (column < boardMatrix[row].length)) {
            boardMatrix[row][column] = element;
        }
    }

   
    /**
     * Este metodo retorna un elemento de la matriz 
     * @param row row position in the matrix
     * @param column column position in the matrix
     * @return element to matrox
     */
    public int getElement(int row, int column) {
        if ((row >= 0) && (column >= 0) && (row < boardMatrix.length) && (column < boardMatrix[row].length)) {
            return boardMatrix[row][column];
        }
        return -1;
    }

    /**
     * this method is responsible to compare element in matrix user
     * @param element element to compare
     * @return validate boolean
     */
    public boolean findElement2(int element) {
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                if (boardMatrix[row][column] == element) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * clean the matrix user
     */
    public void clear() {
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                boardMatrix[row][column] = 0;
            }
        }
    }

    /**
     * this method sum the elements to matrix
     */
    public int getMatrixSum() {
        int sum = 0;
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                sum += boardMatrix[row][column];
            }
        }
        return sum;
    }

    /**
     * this method is responsible for the blows to the matrix
     * @param hitRow hit row in the matrix
     * @param hitColumn hit column int the matrix
     * @return charFound return char
     */
    public char boardHit(int hitRow, int hitColumn) {
        char charFound = '0';

        if (boardMatrixSystem[hitRow][hitColumn] == 's') { // 's' si encuentra un barco "ship"
            boardMatrixSystem[hitRow][hitColumn] = 'x'; //las casillas que contengan 'x' ya no seran jugables
            charFound = 's';
            countHits--;
            //llamar metodo para validar si gano
        } else {
            if (boardMatrixSystem[hitRow][hitColumn] == 'w') { // 'w' si golpea agua "water"
                boardMatrixSystem[hitRow][hitColumn] = 'x';//las casillas que contengan 'x' ya no seran jugables
                charFound = 'w';
            } else {
                if ((boardMatrixSystem[hitRow][hitColumn] == '1')) { // 'i' si encuentra una "incognita"
                    boardMatrixSystem[hitRow][hitColumn] = 'x';//las casillas que contengan 'x' ya no seran jugables
                    charFound = '1';
                } else {
                    if (boardMatrixSystem[hitRow][hitColumn] == '2') {
                        charFound = '2';
                    } else {
                        if (boardMatrixSystem[hitRow][hitColumn] == '3') {
                            charFound = '3';
                        } else {
                            if (boardMatrixSystem[hitRow][hitColumn] == '4') {
                                charFound = '4';
                            } else {
                                if (boardMatrixSystem[hitRow][hitColumn] == '5') {
                                    charFound = '5';
                                } else {
                                    if (boardMatrixSystem[hitRow][hitColumn] == '6') {
                                        charFound = '6';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return charFound;
    }

   /**
    * This method count hits in the matrix
    * @return winner boolean
    */
    public boolean getCounthits() {
        boolean winner = false;
        if (countHits == 0) {
            winner = true;
        } else {
            winner = false;
        }
        return winner;
    }

    /**
     * Este metodo se encarga de validar si el golpe del barco dio en agua, una incognita, o un barco
     * @param boardHit
     * @return 
     */
    public boolean extraHit(char boardHit) {

        boolean keepGoing = false; //determina si el turno continua

        if (boardHit == 's') { // 's' continua el turno
            keepGoing = true;
        } else {
            if (boardHit == 'w') { // 'w' pasa al turno del otro jugador
                keepGoing = false;
            } else {
                if ((boardHit == '1') || (boardHit == '2') || (boardHit == '3') || (boardHit == '4')
                        || (boardHit == '5') || (boardHit == '6')) { // 'i++' continua el turno
                    keepGoing = false;
                }
            }
        }
        return keepGoing;
    }

    /**
     * metodo encargado de los ataques del sistema hacia el usuario
     * @return char
     */
    public char boardHit() {
        int randomRow = 0;
        int randomColumn = 0;

        Random r = new Random();

        randomRow = r.nextInt(14);
        setRowSys(randomRow);
        randomColumn = r.nextInt(14);
        setColumnSys(randomColumn);

        char charFound = '0';//

        if (boardMatrix[randomRow][randomColumn] == 's') { // 's' si encuentra un barco "ship"
            boardMatrix[randomRow][randomColumn] = 'x'; //las casillas que contengan 'x' ya no seran jugables
            charFound = 's';
            countHits--;
            //llamar metodo para validar si gano
        } else {
            if (boardMatrix[randomRow][randomColumn] == 'w') { // 'w' si golpea agua "water"
                boardMatrix[randomRow][randomColumn] = 'x';//las casillas que contengan 'x' ya no seran jugables
                charFound = 'w';
            } else {
                if ((boardMatrix[randomRow][randomColumn] == '1')) { // 'i' si encuentra una "incognita"
                    boardMatrix[randomRow][randomColumn] = 'x';//las casillas que contengan 'x' ya no seran jugables
                    charFound = '1';
                } else {
                    if (boardMatrix[randomRow][randomColumn] == '2') {
                        charFound = '2';
                    } else {
                        if (boardMatrix[randomRow][randomColumn] == '3') {
                            charFound = '3';
                        } else {
                            if (boardMatrix[randomRow][randomColumn] == '4') {
                                charFound = '4';
                            } else {
                                if (boardMatrix[randomRow][randomColumn] == '5') {
                                    charFound = '5';
                                } else {
                                    if (boardMatrix[randomRow][randomColumn] == '6') {
                                        charFound = '6';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return charFound;
    }

    /**
     * crea la primera matriz del usuario sin barcos
     */
    public void firstBoard() {
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                boardMatrix[row][column] = 'w';
            }
        }
    }

    /**
     * crea la primera matriz del siistema sin barcos
     */
    public void firstBoardSystem() {
        for (int row = 0; row < boardMatrixSystem.length; row++) {
            for (int column = 0; column < boardMatrixSystem[row].length; column++) {
                boardMatrixSystem[row][column] = 'w';
            }
        }
    }

    /***
     * este metodo posiciona un punto del barco
     * @param row fila de la matriz
     * @param column columna de la matriz
     */
    public void createMatrixShips(int row, int column) {
        boardMatrix[row][column] = 's';
    }

    
    /**
     * imprime la matriz del usuario
     * @return devuelve el string en forma de matriz
     */
    public String printBoard() {
        String txt = "";
        for (int row = 0; row < boardMatrix.length; row++) {
            for (int column = 0; column < boardMatrix[row].length; column++) {
                txt += boardMatrix[row][column] + " ";
                System.out.print(boardMatrix[row][column] + " ");
            }
            System.out.println("");
            txt += "\n";
        }
        return txt;
    }

    /**
     * Este metodo es el encargado de imprimir la matriz del sistema
     */
    public void printBoardSystem() {
        String txt = "";
        for (int row = 0; row < boardMatrixSystem.length; row++) {
            for (int column = 0; column < boardMatrixSystem[row].length; column++) {
                System.out.print(boardMatrixSystem[row][column] + " ");
            }
            System.out.println("");
            txt += "\n";
        }
    }
    
    /**
     * metodo que contine una validacion del sistema fila para poner el segundo barco de 3
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipThreeRow(int row, int column) {
        int numDim = 3;
        int contador = 0;
        while (numDim >= 1) {
            if (boardMatrixSystem[row][column] == 'w') {
                contador++;
                column++;
                numDim--;
            } else {
                return false;
            }
        }
        column = column - 3;
        if (contador == 3) {
            for (int i = 0; i < 3; i++) {
                boardMatrixSystem[row][column] = 's';
                column++;
            }
            return true;
        } else {
            return false;
        }

    }

    /**
     * metodo que contine una validacion del sistema columna para poner el segundo barco de 3
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipThreeColumn(int row, int column) {
        int numDim = 3;
        int contador = 0;
        while (numDim >= 1) {
            if (boardMatrixSystem[row][column] == 'w') {
                contador++;
                row++;
                numDim--;
            } else {
                return false;
            }
        }
        row = row - 3;
        if (contador == 3) {
            for (int i = 0; i < 3; i++) {
                boardMatrixSystem[row][column] = 's';
                row++;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * valida las posiciones de la fila de la matriz
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @param DIM tamaño del barco
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipPosRowUser(int row, int column, int DIM) {
        int prueba = column;
        int rum = 0;
        int numDim = DIM;
        int contador = 0;
        while (numDim >= 1) {
            if ((boardMatrix[row][prueba] == 'w')) {
                prueba++;
                contador++;
                numDim--;
            } else {
                return false;
            }
        }
        if (contador == DIM) {
            for (int i = 0; i < DIM; i++) {
                boardMatrix[row][column] = 's';
                column++;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * metodo que valida las posiciones de la columna de la matriz
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @param DIM tamaño del barco
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipPosColumnUser(int row, int column, int DIM) {
        System.out.println(DIM + "imprimir DIM");
        int prueba = row;
        int rum = 0;
        int numDim = DIM;
        int contador = 0;
        while (numDim >= 1) {
            if ((boardMatrix[prueba][column] == 'w')) {
                System.out.println("qqqqq");
                System.out.println(prueba);
                prueba++;
                contador++;
                numDim--;
            } else {
                return false;

            }
        }
        if (contador == DIM) {
            System.out.println("fffff");
            for (int i = 0; i < DIM; i++) {
                boardMatrix[row][column] = 's';
                row++;
            }
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * metodo que valida las posiciones de la fila de la matriz del sistema
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @param DIM tamaño del barco
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipPosRow(int row, int column, int DIM) {
        int prueba = column;
        int rum = 0;
        int numDim = DIM;
        int contador = 0;
        while (numDim >= 1) {
            if ((boardMatrixSystem[row][prueba] == 'w')) {
                prueba++;
                contador++;
                numDim--;
            } else {
                return false;

            }
        }
        if (contador == DIM) {
            for (int i = 0; i < DIM; i++) {
                boardMatrixSystem[row][column] = 's';
                column++;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * metodo que valida las posiciones de la columna de la matriz del sistema
     * @param row posicion de la fila de la matriz
     * @param column posicion de la columna de la matriz
     * @param DIM tamaño del barco
     * @return boolean devuelve que pueda poner el barco
     */
    public boolean validateShipPosColumn(int row, int column, int DIM) {
        int prueba = row;
        int rum = 0;
        int numDim = DIM;
        int contador = 0;
        while (numDim >= 1) {
            if ((boardMatrixSystem[prueba][column] == 'w')) {
                prueba++;
                contador++;
                numDim--;
            } else {
                return false;

            }
        }
        if (contador == DIM) {
            for (int i = 0; i < DIM; i++) {
                boardMatrixSystem[row][column] = 's';
                row++;
            }
            return true;
        } else {
            return false;
        }
    }
    int countRandom = 5;

    
    /**
     * Este metodo se encarga de posicionar los barcos de manera aleatoria en el cual
     * si el barco se puede generar, con un random busca una posicion y en este valida si se
     * puede posicionar o no.
     */
    public void loadShipsSystem() {
        firstBoardSystem();
        printBoardSystem();
        int deftam = 10;
        int randomRow = 0;
        int randomColumn = 0;
        int randomHV = 0;
        int index = 0;
        int dimShip = 5;
        Random r = new Random();
        boolean validate = true;
        while (index < 4) {//lo repite 5 veces
            validate = true;
            while (validate) { //la validacion para que busque random si no lo puede posicionar
                randomRow = r.nextInt(14);
                randomColumn = r.nextInt(14);
                randomHV = r.nextInt(2);
                printBoardSystem();
                System.out.println("0");
                if (randomHV == 0) { //si es horizontal
                    System.out.println("1");
                    printBoardSystem();
                    if (randomColumn <= deftam) { //que no se pase del tamaño limite del barco
                    
                        if (validateShipPosRow(randomRow, randomColumn, countRandom)) {//posiciona el barco o no
                            index++;
                            countRandom--;
                            deftam++;
                            validate = false;
                        printBoardSystem();
                        System.out.println("2");
                        } else {
                        printBoardSystem();
                        System.out.println("3");
                            System.out.println("hay un barco en esta posicion");
                        }
                    } else {
                        System.out.println("el barco no cabe en esa posicion");
                    }
                } else {
                    if (randomRow <= deftam) {
                        if (validateShipPosColumn(randomRow, randomColumn, countRandom)) {
                            deftam++;
                            index++;
                            countRandom--;
                            validate = false;
                        } else {
                            System.out.println("hay un barco en esta posicion");
                        }
                    } else {
                        System.out.println("el barco no cabe en esa posicion");
                    }
                }

            }
        }
        countRandom = 5;
        boolean validateShip3 = true; // posiciona el barco de 3
        while (validateShip3) {//lo repite hasta que pueda posicionar un barco
            randomRow = r.nextInt(14);
            randomColumn = r.nextInt(14);
            randomHV = r.nextInt(2);
            if (randomHV == 0) {
                if (randomColumn <= 12) {
                    if (validateShipThreeRow(randomRow, randomColumn)) {
                        validateShip3 = false;
                        break;
                    } else {
                        System.out.println("hay un barco desde una posicion");
                    }

                } else {
                    System.out.println("el barco no cabe en esa posicion");
                }

            } else {

                if (randomColumn <= 12) {//si no se sobre pasa
                    if (validateShipThreeColumn(randomRow, randomColumn)) {//posiciona o no el barco
                        validateShip3 = false;
                        break;
                    } else {
                        System.out.println("hay un barco desde una posicion");
                    }

                } else {
                    System.out.println("el barco no cabe en esa posicion");
                }

            }

        }
        incognite();
        printBoardSystem();
    }

    
    /**
     * este metodo posiciona las incognitas aleatoriamente
     */
    public void incognite() {
        int random = 0;
        int randomRow = 0;
        int randomColumn = 0;
        Random rand = new Random();
        int validate = 1;
        String num = "";
        while(validate <= 6) {
            num = validate + ""; 
            randomRow = rand.nextInt(14);
            randomColumn = rand.nextInt(14);
            if((boardMatrixSystem[randomRow][randomColumn] != 's')) {
                boardMatrixSystem[randomRow][randomColumn] = num.charAt(0);
                random++;
                validate++;
            }
        }
    }
}