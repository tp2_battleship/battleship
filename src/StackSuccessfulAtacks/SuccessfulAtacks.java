/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StackSuccessfulAtacks;

/**
 *  Clase encargada de manejar los datos que se van a cargar en la pila(nodos)
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class SuccessfulAtacks {
    String time;
    int row;
    int column;

    public SuccessfulAtacks(String time, int row, int column) {
        this.time = time;
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return  "{ time=" + time + ", row=" + row + ", column=" + column + '}';
    }

    
    
    
}
