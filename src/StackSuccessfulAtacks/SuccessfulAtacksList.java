/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StackSuccessfulAtacks;

import battleship.PlayFrame;

/**
 * Clase encargada de manejar la lista, contiene los metodos encargados de manejar este
 * esta clase se encarga de guardar los ataques del jugador
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class SuccessfulAtacksList {

    private SuccessfulAtacksNode top;

    public SuccessfulAtacksList() {
    }
            
    public SuccessfulAtacksList(SuccessfulAtacksNode first) {
        this.top = null;
    }

    public SuccessfulAtacksNode getTop() {
        return top;
    }

    public void setTop(SuccessfulAtacksNode top) {
        this.top = top;
    }
    
    
    /**
     * Metodo que inserta elementos en la parte superior de la pila (de primero)
     * @param dato el elemento que sera agregado a la pila
     */
    public void insertAtTheTop(SuccessfulAtacks dato) { //add()
        SuccessfulAtacksNode newNode = new SuccessfulAtacksNode(dato);
        newNode.setFollowing(this.top);
        this.top = newNode;
    }

    /**
     * Metodo que extrae el elemento que se encuentra en la cima de la pila
     * y lo retorna
     * @return elemento extraido
     */
    public SuccessfulAtacks extractTop() { //pop()
        SuccessfulAtacks dato = top.getSuccessfulAtack();
        this.top = top.getFollowing();
        return dato;
    }
    
     
    /**
     * Metodo que cuenta la cantidad de nodos que hay en la pila
     * @return cantidad de elementos
     */
    public int countElements() {
        int count = 0;
        SuccessfulAtacksNode node = top;
        while (node != null) {
            count++;
            node = node.getFollowing();
        }
        System.out.println("Numero de datos en la pila: " + count);
        return count;
    }
    
    /**
     * Metodo que comprueba si la pila contiene elementos
     *
     * @return valor de verdad dependiendo si hay elementos o no
     */
    public boolean isEmpty() { //empty()
        if (top == null) {
            System.out.println("Esta vacia");
            return true;
        } else {
            System.out.println("No esta vacia");
            return false;
        }
    }
    
    /**
     * Metodo que imprime un string con los elementos de la lista
     * @return hilera con los elementos de la lista
     */
    public String toString() {
        
        String list = "";
        
        SuccessfulAtacksNode node = this.top;
        
        while (node != null) {
            
            list = list + node.toString();
            node = node.getFollowing();
        }
        
        return list;
    }
}
