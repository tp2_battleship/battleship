/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StackSuccessfulAtacks;

/**
 * Esta clase se encarga de crear los nodos de la pila(lista simple)
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class SuccessfulAtacksNode {
    
    private SuccessfulAtacksNode following;
    private SuccessfulAtacks successfulAtack;

    
    public SuccessfulAtacksNode(SuccessfulAtacks successfulAtack) {
        
        this.following = following;
        this.successfulAtack = successfulAtack;
    }

    public SuccessfulAtacksNode getFollowing() {
        return following;
    }

    public void setFollowing(SuccessfulAtacksNode following) {
        this.following = following;
    }

    public SuccessfulAtacks getSuccessfulAtack() {
        return successfulAtack;
    }

    public void setSuccessfulAtack(SuccessfulAtacks successfulAtack) {
        this.successfulAtack = successfulAtack;
    }

    @Override
    public String toString() {
        return "SuccessfulAtack{"  + successfulAtack + '}' + "  \n";
    }
    
    
    
    
    
}
