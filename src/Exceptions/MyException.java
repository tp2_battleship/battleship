/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 * Esta clase es encargada del manejo de algunas excepciones
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class MyException extends Exception{
    
    
    
    public MyException(String string) {
        super(string);
    }
    
}
