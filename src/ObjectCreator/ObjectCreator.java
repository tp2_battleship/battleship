/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectCreator;


import StackSuccessfulAtacks.SuccessfulAtacks;
import SystemRandom.RegisterMoves;

import battleship.PlayFrame.Chronometer;

import csv.Player;


/**
 * Se encarga de crear los objetos de de los randoms y las pilas.
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class ObjectCreator {
          
    /**
     * registra los movimientos del usuario
     * @param row filas para el registro aleatorio
     * @param column columna para el registro aleatorio
     * @return una instancia del metodo
     */
    public RegisterMoves createMoves(int row, int column) {
        return new RegisterMoves(row, column, "00:00:00", '0');
    }
    
    /**
     * Crea un ataque para cargarlo a la pila
     * @param time String del tiempo
     * @param row fila del golpe
     * @param column columna del golpe
     * @return retorna un nodo a guardar
     */
    public SuccessfulAtacks createAtack(String time, int row, int column) {
        return new SuccessfulAtacks(time, row, column);
    }
    
    /**
     * metodo encargado de crear un jugador
     * @param nickName String usuario
     * @param password String contraseña
     * @return retorna un jugador
     */
    public Player player(String nickName, String password) {
        return new Player(nickName, password);
    }
    

}
