package NewRecords;

import battleship.*;


public class Menu {

    public static void mostrarMenu() {
        final int ADDPLAYER = 0;
        final int DELETEPLAYER = 1;
        final int SEE_ALL = 2;
        final int EXIT = 3;

        String[] categoryOption = {"Add record", "delete record", "see all records", "Exit"};
        boolean exit = false;

        do {
            int option = IOManager.showPlusMessage("Soccer manager", categoryOption);
            switch (option) {
                case ADDPLAYER:
                    int time = IOManager.getInt("Digite el time");
                    String nickname = IOManager.getString("Digite el nickname del jugador");
                    
                    Battleship.manager.addRecord(new Record(time, nickname));
                    break;

                case DELETEPLAYER:
                    Battleship.manager.deleteRecord();
                    IOManager.showMessage("Se ha eliminado el ultimo jugador creado");
                    break;

                case SEE_ALL:
                    IOManager.showMessage(Battleship.manager.getStringVector());
                    break;

                case EXIT:
                    IOManager.showMessage("Gracias por utilizar el sistema");
                    exit = true;
                    break;
            }
        } while (!exit);

    }

}
