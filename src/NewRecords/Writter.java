package NewRecords;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import battleship.*;
/**
 *
 * @author Kevin
 */
public class Writter {

    private ObjectOutputStream oos;
    private String nameOfFile;

    public Writter(String nameOfFile) {
        this.nameOfFile = nameOfFile;
    }

    public boolean openFile() {

        try {
            oos = new ObjectOutputStream(new FileOutputStream(nameOfFile));
            return true;
        } catch (IOException ex) {
            System.out.println("Se genero un: FileNotFoundException");;
            return false;
        }

    }

    /**
     * Escribe un objeto tipo Record en el archivo binario
     * @param record 
     */
    public void writeFile(Record record) {
        try {
            oos.writeObject(record);
        } catch (IOException e) {
            System.out.println("Se genero una IOException");
        }
    }

    /**
     * Metodo que escribe el elemento encontrado del vector, al archivo binario
     * los escribe en orden inciando en la pocision cero
     */
    public void extractWriter() {
        for (int i = 0; i < Battleship.manager.getCounter(); i++) {
            writeFile(Battleship.manager.getElement(i));
        }
    }

    public void close() {
        try {
            oos.flush();
            oos.close();
        } catch (IOException ex) {
            System.out.println("Se genero una IOException");
        }
    }

}
