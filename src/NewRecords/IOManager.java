package NewRecords;



import javax.swing.JOptionPane;

public class IOManager {

    public static int getInt(String message) {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, message));
        return number;
    }

    public static int showPlusMessage(String message, String[] stringVector) {
        int option = JOptionPane.showOptionDialog(null, message, null, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, stringVector, null);
        return option;
    }

    public static String getString(String message) {
        String hilera = JOptionPane.showInputDialog(null, message);
        return hilera;
    }

    public static void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
}
