package NewRecords;



import java.io.Serializable;

public class Record implements Serializable  {
  
    private int time;
    private String nickName;
    

    public Record() {
    }
        
    public Record(int time, String nickName) {
        this.time = time;
        this.nickName = nickName;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    
    
    /**
     * calcula la suma de todos los segundos en un record
     * @return suma de segundos
     */
    public int calculateSeconds(){
           int totalSeconds = time;
           return totalSeconds;
        }

//    @Override
//    public String toString() {
//        return "Record: " + "time: " + time + ", nickName: " + nickName;
//    }
    
    @Override
    public String toString() {
            return "Record: " + time/3600 + " : " + time /60 + " : " + time%60  + ", nickName: " + nickName;
    }
    
    
}
