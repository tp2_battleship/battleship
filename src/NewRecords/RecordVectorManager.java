package NewRecords;


public class RecordVectorManager {

    private Record[] recordsVector;
    private int counter = 0;
    
    public RecordVectorManager() {
    }

    public RecordVectorManager(int length) {
        this.recordsVector = new Record[length];
    }

    /**
     * Metodo que agrega un nuevo al archivo,
     * compara en el vector si el nickname existe
     * si existe y el nuevo tiempo es menor, entonces lo reemplaza
     * @param record 
     */
    public void addRecord(Record record) {
        if (counter >= 0 && counter < recordsVector.length) {
            recordsVector[counter] = record;
            counter++;
            orderByTime();
        }
    }

    /**
     * Metodo que elimina el ultimo record del archivo
     */
    public void deleteRecord() {
        if (counter > 0) {
            recordsVector[counter - 1] = null;
            counter--;
        }
    }

    public Record getElement(int index) {
        if (counter >= 0 && counter < recordsVector.length) {
            return recordsVector[index];
        } else {
            return null;
        }
    }
    
    /**
     * Imprime la lista de records desde el vector
     * @return lista de 10 mejores records
     */
    public String getStringVector() {
        String text = "";
        for (int i = 0; i < recordsVector.length; i++) {
            if (recordsVector[i] != null) {
                text += recordsVector[i] + "\n";
            }
        }
        return text;
    }
    
    public int getCounter() {
        return counter;
    }
    
    /**
     * Metodo que ordena los records de menor a mayor de acuerdo al tiempo
     */
    public void orderByTime() {
        Record aux;
        for (int i = 1; i < counter; i++) {
            for (int j = 0; j < counter - i; j++) {
                if ((recordsVector[j].getTime()) > (recordsVector[j + 1].getTime())) {
                    aux = recordsVector[j];
                    recordsVector[j] = recordsVector[j + 1];
                    recordsVector[j + 1] = aux;
                }
            }
        }
    }

}
