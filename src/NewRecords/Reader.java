package NewRecords;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import battleship.*;

/**
 *
 * @author Kevin
 */
public class Reader {

    private ObjectInputStream ois;
    private String nameOfFile;

    public Reader(String nameOfFile) {
        this.nameOfFile = nameOfFile;
    }

    public boolean openFile() {
        try {
            ois = new ObjectInputStream(new FileInputStream(nameOfFile));
            return true;
        } catch (IOException ex) {
            System.err.println("Se genera un error de tipo IOException en el metodo readObject");
            return false;

        }
    }

    public Record readRegistre() throws ClassNotFoundException {
        Record record;
        try {
            if ((record = ((Record) ois.readObject())) != null) {

                return record;
            }
        } catch (IOException e) {
            System.out.println("Se genero un: IOException");
            return null;
        }
        return null;
    }

    public void readFile() {
        boolean tempBoolean = true;
        Record record;
        try {
            while (tempBoolean != false) {

                record = readRegistre();
                if (record != null) {

                    Battleship.manager.addRecord(record);
                } else {
                    tempBoolean = false;
                }
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("se genera un error de tipo ClassNotFoundException");

        }
    }

    public void close() {
        try {
            ois.close();
        } catch (IOException ex) {
            System.out.println("Se genero un: IOException");
        }
    }

}
