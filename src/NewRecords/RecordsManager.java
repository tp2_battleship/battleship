/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewRecords;


import battleship.*;

/**
 *
 * @author Usuario
 */
public class RecordsManager {
    
    //public static RecordVectorManager manager = new RecordVectorManager(10);
    
    public static void newRecord(int time, String nickname) {

        Battleship.manager.addRecord(new Record(time, nickname));

    }
    
    public static void deleteRecord() {

        Battleship.manager.deleteRecord();
        IOManager.showMessage("Se ha eliminado el ultimo jugador creado");
    }

    public static String printRecords() {
        String hilera = "";
        hilera = Battleship.manager.getStringVector();
        return hilera;
    }
}
