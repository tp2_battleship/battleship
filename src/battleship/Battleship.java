/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;


import static battleship.Battleship.manager;
import NewRecords.Menu;
import NewRecords.Reader;
import NewRecords.RecordVectorManager;
import NewRecords.RecordsManager;
import NewRecords.Writter;
import csv.ArrayListPlayer;
import csv.Player;
import csv.WriteCsv;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Este es la clase main carga el csv pone la musica y reproduce la parte grafica
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class Battleship {
    public static RecordVectorManager manager = new RecordVectorManager(15);
    public static RecordsManager mangerRec = new RecordsManager();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //carga el arrayList con el csv
        ArrayListPlayer classArray = ArrayListPlayer.getInstance();
        String fileName = "PlayerFichero.csv";
        ArrayList<Player> arrayLoad = new ArrayList<Player>();  
        WriteCsv wc = new WriteCsv();
        
        arrayLoad = wc.readCsvFile(fileName); //lee el array
        classArray.setArrayListPlayer(arrayLoad); //lo guarda en la clase array
        classArray.print();
        //abre la nueva ventana
        VentanaInicial ventana = new VentanaInicial();
        ventana.setVisible(true);
        
        
        Reader read = new Reader("test.txt");

        boolean tempBoolean = read.openFile();
        if (tempBoolean == true) {
            read.readFile();
            
            read.close();

        }
        System.out.println("Records:" + mangerRec.printRecords());
        //manager.addRecord(new Record(time, nickname));
       // Menu.mostrarMenu();
        //mangerRec.printRecords();
        
       
        
        
        //reproduce el sonido del juego
        try {
            Clip sonido = AudioSystem.getClip();
            File a = new File("src/sounds/themeInitial.wav");

             sonido.open(AudioSystem.getAudioInputStream(a));
                sonido.start();
            
            
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Battleship.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(Battleship.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Battleship.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
