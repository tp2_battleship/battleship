/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;


import Logic.MatrixLogic;
import NewRecords.Reader;
import NewRecords.RecordVectorManager;

import NewRecords.Writter;
import ObjectCreator.ObjectCreator;
import StackSuccessfulAtacks.SuccessfulAtacksList;
import SystemRandom.RegisterMovesHandler;
import csv.ArrayListPlayer;
import csv.WriteCsv;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *  Esta clase se encarga de manejar la ventana de jugar con el usuraio
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class PlayFrame extends javax.swing.JFrame implements ActionListener{
    ArrayListPlayer classArray = ArrayListPlayer.getInstance();//arrayList usuarios
    WriteCsv wc = new WriteCsv(); //Usuarios - texto
    RegisterMovesHandler sysMoves = new RegisterMovesHandler(); //Aleatorios movimientos del sistema
    SuccessfulAtacksList atackList = new SuccessfulAtacksList(); //listas enlazadas movimientos jugador
    ObjectCreator createObject = new ObjectCreator(); //Creador de objetos
    //public static RecordsManager mangerRec = new RecordsManager();//Binarios records
    //public static RecordVectorManager vectorRecords = new RecordVectorManager();
    
    boolean validateGameTurn = true; //validates who plays
    public static String nick;
    
    JLabel[][] label = new JLabel[15][15];//matrix user
    JButton[][] button = new JButton[15][15]; //matrix system
    int victory = 0;
    int victorySystem = 0;
    
    //tamano de la matriz
    public static final int COLUMN = 15;
    public static final int ROW = 15;
    
    MatrixLogic hitMatrix = MatrixLogic.getInstance();//singleton
    
    boolean continuePlaying = true; //dice si continua jugando, si es false hay que llamar a que el sistema juegue y hay qe bloquedar el mouse
    Chronometer cr = new Chronometer();//instancia del tiempo hilo
    /**
     * Creates new form Prueba
     */
    public PlayFrame() {
        initComponents();
        //Color JFrame
        this.getContentPane().setBackground(Color.BLACK);
        cr.start();
        //i and j position in the label
        int i = 100;
        int j = 100;
        System.out.println("***************+");
        hitMatrix.printBoard();
        System.out.println("*************");
        hitMatrix.printBoardSystem();
        System.out.println("********************");
        
        if (sysMoves.open("moves.txt")) { //abre el archivo de jugadas del sistema y lo llena con datos por defecto
            sysMoves.CreateEmptyRegisters(); //Crea un archivo aleatorio al inicio del juego, el cual esta con datos predefinidos
            //System.out.println("" + sysMoves.deployAllRegisters());
            sysMoves.close();
        } else {
            System.out.println("No se pudo abrir");
        }
       
        for(int row = 0; row < label.length; row++) {//positioned rows in the board system
            for(int column = 0; column < label[row].length; column++) {//positioned columns in the board system
                    label[row][column] = new JLabel();
                    label[row][column].setBounds(i, j, 25, 25);
                    ImageIcon image = new ImageIcon("src/images/fondoBoton.jpg");
                    Icon icon = new ImageIcon(image.getImage().getScaledInstance(label[row][column].getWidth(), label[row][column].getHeight(), Image.SCALE_DEFAULT));
                    label[row][column].setIcon(icon);
                    i += 25;
                    add(label[row][column]);       
            }
            i = 100;
            j += 25;
        }
        
        int l = 500+40+10;
        int k = 100;

        for(int row = 0; row < button.length; row++) {//positioned rows in the board user
            for(int column = 0; column < button[row].length; column++) {//positioned columns in the board user
                    button[row][column] = new JButton();
                    button[row][column].setBounds(l, k, 25, 25);
                    ImageIcon image = new ImageIcon("src/images/fondoBoton.jpg");
                    Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));
                    button[row][column].setIcon(icon);
                    l += 25;
                    add(button[row][column]);
                    button[row][column].addActionListener(this);
            }
            l = 500+40+10;
            k += 25;
        }
    }
    
    /**
     * evento encargado de jugar contra el sistema
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        char characterHit = 'x';
        for (int row = 0; row < button.length; row++) {
            for (int column = 0; column < button[row].length; column++) {
                if (e.getSource() == button[row][column]) {
                    
                    try {
                        playUser(row, column);
                    } catch (IOException ex) {
                        Logger.getLogger(PlayFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public void updateRecords() {

        String recordsList = Battleship.mangerRec.printRecords();

        Chronometer timer = new Chronometer();
        String actualNickname = UserLabel.getText();
        String times = labelTiempo.getText();
        int accSeconds = times.charAt(4);
        int accMinutes = times.charAt(2);
        int accHours = times.charAt(0);
        int accTime = accSeconds + accMinutes + accHours;
        Battleship.mangerRec.newRecord(accTime, actualNickname);
        
       // RecordsManager.newRecord(accTime, actualNickname);
                //mangerRec.newRecord(time, actualNickname);

    }
    
    
    //public static RecordsManager mangerRec = new RecordsManager();
    /**
     *  Metodo que dice si el jugador acerta un golpe contra el sistema y si puede volver a tirar
     * @param row
     * @param column 
     */   
    public void playUser(int row, int column) throws IOException {
        char charHit = hitMatrix.boardHit(row, column);
        boolean playValidate = hitMatrix.extraHit(charHit);
        
        if(playValidate) {
            ImageIcon image = new ImageIcon("src/images/bolagifGolpe.gif");
            Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));
            button[row][column].setIcon(icon);
            atackList.insertAtTheTop(createObject.createAtack(labelTiempo.getText(), row, column)); //agrega el ataque exitoso a la pila
            System.out.println("Lista Nodos: " + atackList.toString());
            victory++;
            button[row][column].removeActionListener(this);

        } else {
            if((charHit != 'w')&&(charHit != 's')) {
                cr.incogniteGood(row, column, (int)labelTiempo.getText().charAt(0), (int)labelTiempo.getText().charAt(1), (int)labelTiempo.getText().charAt(2), charHit);
            } else {
                ImageIcon image = new ImageIcon("src/images/bolaGif.gif");
                Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));
                button[row][column].setIcon(icon);
                button[row][column].removeActionListener(this);
            }
            playSystem();
            if(victory == 17) {
                
                Congratulations congrat = new Congratulations();
                congrat.setVisible(true);
                updateRecords();
                dispose();
        }
        }
    }
    
    /**
     * Metodo que dice si el sistema acerta un golpe contra el jugador y si puede volver a tirar
     */
    public void playSystem() {
        char charHit = hitMatrix.boardHit();
        hitMatrix.getStringMatrix();
        boolean playValidate = hitMatrix.extraHit(charHit);
        do {
            if (playValidate) {
                ImageIcon image = new ImageIcon("src/images/bolagifGolpe.gif");
                Icon icon = new ImageIcon(image.getImage().getScaledInstance(label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].getWidth(), label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].getHeight(), Image.SCALE_DEFAULT));
                label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].setIcon(icon);
                
                victorySystem++;
                
                
            } else {
                ImageIcon image = new ImageIcon("src/images/bolaGif.gif");
                Icon icon = new ImageIcon(image.getImage().getScaledInstance(label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].getWidth(), label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].getHeight(), Image.SCALE_DEFAULT));
                label[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].setIcon(icon);
                //cr.incogniteGood(hitMatrix.getRowSys(), hitMatrix.getColumnSys(), (int)labelTiempo.getText().charAt(0), (int)labelTiempo.getText().charAt(1), (int)labelTiempo.getText().charAt(2), charHit);
                button[hitMatrix.getRowSys()][hitMatrix.getColumnSys()].removeActionListener(this);
            }
            
            charHit = hitMatrix.boardHit();
            playValidate = hitMatrix.extraHit(charHit);
           //abre el archivo
            if (sysMoves.open("moves.txt")) {
                //sysMoves.resetChar(hitMatrix.getRowSys(), hitMatrix.getColumnSys(), charHit);
                sysMoves.resetTime(labelTiempo.getText(), hitMatrix.getRowSys(), hitMatrix.getColumnSys(), charHit);
                //System.out.println("" + sysMoves.deployAllRegisters());
                sysMoves.close();
            } else {
                System.out.println("No se pudo abrir");
            }

        }while(playValidate);
        if(victorySystem == 17) {
            
            Congratulations congrat= new Congratulations();
            congrat.setVisible(true);
                        updateRecords();
            dispose();
        }

    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelTiempo = new javax.swing.JLabel();
        UserLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        exitButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        labelTiempo.setFont(new java.awt.Font("Monotype Corsiva", 0, 36)); // NOI18N
        labelTiempo.setForeground(new java.awt.Color(0, 0, 255));

        UserLabel.setForeground(new java.awt.Color(51, 51, 255));

        jLabel1.setText("jLabel1");

        exitButton.setText("EXIT");
        exitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(601, 601, 601)
                        .addComponent(labelTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(491, 491, 491)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(785, 785, 785)
                        .addComponent(exitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(UserLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(84, 84, 84))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(UserLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(171, 171, 171)
                .addComponent(jLabel1)
                .addGap(226, 226, 226)
                .addComponent(exitButton)
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        FirstMenuFrame first = new FirstMenuFrame();
        first.setVisible(true);
        dispose();
    }//GEN-LAST:event_exitButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        close();
    }//GEN-LAST:event_formWindowClosing
    
    public void labelUser(String nick) {
        System.out.println("nick " + nick);
        UserLabel.setText(nick);
    }
    
    /**
     * Este metodo valida que cuando presione x guarde en el fichero
     */
    public void close() {
        try {
            Object[] opciones = {"ACEPT", "CANCEL"};
            int eleccion = JOptionPane.showOptionDialog(rootPane, "Are you sure you want to quit ?", "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, opciones, "ACEPT");
            if (eleccion == JOptionPane.YES_OPTION) {
                wc.writeCsvFile("PlayerFichero.csv", classArray.getListPlayer());
                Writter writter = new Writter("test.txt");
               
                System.out.println(writter.openFile());
                writter.extractWriter();
                writter.close();
                System.exit(0);
            } else {
            }
   
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PlayFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PlayFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PlayFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PlayFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PlayFrame().setVisible(true);
                
            }
        });
       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel UserLabel;
    private javax.swing.JButton exitButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel labelTiempo;
    // End of variables declaration//GEN-END:variables
    
    public class Chronometer extends Thread {

        int seconds = 0;
        int minutes = 0;
        int hours = 0;
              
        MatrixLogic logic = MatrixLogic.getInstance();

        public Chronometer() {

        }

        public Chronometer(int seconds, int minutes, int hours) {
            this.seconds = seconds;
            this.minutes = minutes;
            this.hours = hours;

        }

        public int getSeconds() {
            return seconds;
        }

        public void setSeconds(int seconds) {
            this.seconds = seconds;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public MatrixLogic getLogic() {
            return logic;
        }

        public void setLogic(MatrixLogic logic) {
            this.logic = logic;
        }

        
        @Override
        public String toString() {
            return hours + ":" + minutes + ":" + seconds;
        }
        

        /**
         * Calcula los segundos totales a partir de segundos, minutos y horas
         * @return cantidad de segundos totales
         */

        public void incogniteGood(int row, int column, int hour, int minut, int second, int randomIncognite) {

            Random r = new Random();
            int sgRandom = 0;
            int randomtam = 2;
            randomtam = r.nextInt(5);
            sgRandom = r.nextInt(2);
            if ((randomIncognite == '1') || (randomIncognite == '2')) {
                ImageIcon image = new ImageIcon("src/images/fondoVentana.gif");
                Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));
                button[row][column].setIcon(icon);
                button[row][column].setEnabled(false);
                if (sgRandom == 0) {
                    if (minut < 1) {
                        if (second < 5) {
                            setSeconds(0);
                        } else {
                            setSeconds(second - 5);
                        }
                    } else {
                        if ((second <= 4) && (second == 4)) {
                            setMinutes(minut - 1);
                            setSeconds(59);
                            setSeconds(second - 1);

                        } else {
                            if ((second < 4) && (second == 3)) {
                                setMinutes(minut - 1);
                                setSeconds(59);
                                setSeconds(second - 2);
                            } else {
                                if ((second < 4) && (second == 2)) {
                                    setMinutes(minut - 1);
                                    setSeconds(59);
                                    setSeconds(second - 3);
                                } else {
                                    if ((second < 4) && (second == 1)) {
                                        setMinutes(minut - 1);
                                        setSeconds(59);
                                        setSeconds(second - 4);
                                    } else {
                                        setSeconds(second - 5);
                                    }
                                }
                            }

                        }
                    }

                } else {
                    if (minut < 1) {
                        if (second < 5) {
                            setSeconds(0);
                        } else {
                            setSeconds(second - 10);
                        }
                    } else {
                        if ((second <= 4) && (second == 4)) {
                            setMinutes(minut - 1);
                            setSeconds(59);
                            setSeconds(second - 5);
                        } else {
                            if ((second < 4) && (second == 3)) {
                                setMinutes(minut - 1);
                                setSeconds(59);
                                setSeconds(second - 6);
                            } else {
                                if ((second < 4) && (second == 2)) {
                                    setMinutes(minut - 1);
                                    setSeconds(59);
                                    setSeconds(second - 7);
                                } else {
                                    if ((second < 4) && (second == 1)) {
                                        setMinutes(minut - 1);
                                        setSeconds(59);
                                        setSeconds(second - 4);
                                    } else {
                                        setSeconds(second - 8);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
         
                    ImageIcon image = new ImageIcon("src/images/bolagif.gif");
                    Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));

                    button[row][column].setIcon(icon);
                    button[row][column].setEnabled(false);
   
            }
        }
        
        /**
     * este metod es encargado de manejar las incognitas malas 
     * @param random recibe un numero aleatorio para decidir cuantas posiciones va a ocultar en la parte grafica
     */
//    
    
    public boolean incogniteBad(int random, int index) {
            
            if ((atackList.isEmpty()) || (index == random)) {
                return false;
            } else {
                atackList.extractTop();
                int row = atackList.getTop().getSuccessfulAtack().getRow();
                int column = atackList.getTop().getSuccessfulAtack().getColumn();
                atackList.extractTop();
                ImageIcon image = new ImageIcon("src/images/fondoVentana.gif");
                Icon icon = new ImageIcon(image.getImage().getScaledInstance(button[row][column].getWidth(), button[row][column].getHeight(), Image.SCALE_DEFAULT));

                button[row][column].setIcon(icon);
                button[row][column].setEnabled(false);

                return incogniteBad(random, index++);
            }

        }
            
        

        public void run() {
             
                    try {
                for (;;) {
                    if (seconds != 59) {
                        seconds++;
                    } else {
                        if (minutes != 59) {
                            seconds = 0;
                            minutes++;
                        } else {
                            hours++;
                            minutes = 0;
                            seconds = 0;
                        }
                    }
                    labelTiempo.setText(hours + ":" + minutes + ":" + seconds);
                    sleep(999);
                }
            } catch (Exception e) {
            }
        }
    }
}
