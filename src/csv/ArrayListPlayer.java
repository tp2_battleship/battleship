/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csv;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Clase encargada de manejar el arrayList donde se cargaran los jugadores con sus respectivas contraseñas
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class ArrayListPlayer {
    private static ArrayListPlayer arrayClass = new ArrayListPlayer();
    private ArrayList<Player> listPlayer = new ArrayList<Player>();//make a arrayList
    private int count = 0; //this variable is a user counter
    
    public ArrayListPlayer() {
    }
    
    
    public static ArrayListPlayer getInstance() {
        if(arrayClass == null) {
            ArrayListPlayer arrayClass = new ArrayListPlayer();
        }
        return arrayClass;
    }
    
    

    public ArrayListPlayer(ArrayList<Player> listPlayer) {
        this.listPlayer = listPlayer;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
    public ArrayList<Player> getListPlayer() {
        return listPlayer;
    }
    

    public void setArrayListPlayer(ArrayList<Player> listPlayer) {
        this.listPlayer = listPlayer;
    }
    
    /**
     * Metodo encargado de añadir un jugador al array
     * @param player
     */
    public void AddPlayer(Player player) {
        this.listPlayer.add(player);
        count++;
    }
    
   /**
    * Este metodo valida si el nickname que recibe por parametrosesta existe en el array
    * @param nickName. String a comparar
    * @return boolean confirma si lo encuentra o no
    */ 
    public boolean containsPlayer(String nickName) {
        for (Player player: listPlayer) {
                if (player.getNickName().equals(nickName)) {
                    return true;
                }
        }
        return false;
    }
    
    /**
     * Este metodo valida si el password que recibe por parametros esta existe en el array
     * @param password String a comparar
     * @param nickName String a comparar nickName
     * @return boolean confirma si lo encuentra o no
     */
    public boolean containsPlayerPass(String password, String nickName) {
        for (Player player: listPlayer) {
            if(player.getNickName().equals(nickName)) {
                if (player.getPassword().equals(password)) {
                    return true;
                }
            }     
        }
        return false;
    }
    
    /**
     * Este metodo imprimi la lista del array
     */
    public void print() {
        if(listPlayer != null) {
            for(Player player : listPlayer) {
                System.out.println("Player: " + player.getNickName() + ", password: " + player.getPassword());
            }
        } else {
            System.out.println("No registered players");
        }
    }
}
