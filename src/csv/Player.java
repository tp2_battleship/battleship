/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csv;

/**
 * Esta clase tiene los atributos del jugador
 * @author Bernardo Gomez
 * @author Jesus Acuna
 */
public class Player {
    private String nickName;
    private String password;
    private String time;
    
    public Player() {
    }

    public Player(String nickName, String password, String time) {
        this.nickName = nickName;
        this.password = password;
        this.time = time;
    }

    public Player(String nickName, String password) {
        this.nickName = nickName;
        this.password = password;
    }

    

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public String toString2() {
        return nickName + "," + password + "," + time;
    }

    @Override
    public String toString() {
        return "Player{" + "nickName=" + nickName + ", password=" + password + '}';
    }
    
    
    
    
}
