/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Clase responsable de leer y escribir un fichero .csv, la contraseña y el nickName del usuario.
 * @author Bernardo Gomez
 * @author Jesus Acuña
 */
public class WriteCsv {
    private static final String COMMA_DELIMETER = ",";//delimitador por coma.
    private static final String NEW_LINE_SEPARATOR = "\n";//linea separador.
    private static final String FILE_HEADER = "NickName,Password,Time"; //cabecera
    //contador para escribir en el .csv
    private static final int NICKNAMEID = 0;
    private static final int PASSWORDID = 1;
     
    /**
     * lee en el archivo y lo carga en un arrayList y lo devuelve
     * @param fileName. nombre del fichero .csv
     * @return arrayPlayer. arrayPlayer is the arrayList of users
     */
    public static ArrayList<Player> readCsvFile(String fileName) {
        ArrayList<Player> arrayPlayer = new ArrayList();//create arrayList
        BufferedReader fileReader = null;
        
        try {    
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileName));
            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(COMMA_DELIMETER);
                if (tokens.length > 0) {
                    Player player = new Player(tokens[NICKNAMEID], tokens[PASSWORDID]);
                    arrayPlayer.add(player);//añade el jugador al array
                }
            }

            for (Player player : arrayPlayer) {
                System.out.println(player.toString());
            }

        } catch (Exception e) {
            System.out.println("Error in CsvFileReader" + e);

        } finally {
            try {
                fileReader.close();
            } catch (Exception e) {
                System.out.println("error in close fileReader" + e);
            }
        }
        return arrayPlayer;
    }
    
    /**
     * metodo responsable de escribir en el archivo .csv
     * @param fileName. fileName is the name of file.
     * @param list. this is user arrayList
     */
    public static void writeCsvFile(String fileName, ArrayList<Player> list) {
        FileWriter fileWriter = null;
        
        try {
            fileWriter = new FileWriter(fileName);

            for (Player player : list) {
                fileWriter.append(player.getNickName());
                fileWriter.append(COMMA_DELIMETER);
                fileWriter.append(player.getPassword());
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            System.out.println("Datos cargados");
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter" + e);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {
                System.out.println("error in close or flush");
            }
        }
    }
}
